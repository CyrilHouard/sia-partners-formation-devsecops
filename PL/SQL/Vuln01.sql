declare
 type ct is ref cursor;
 crs ct;
 id number;
 stmt varchar2(512);
 title varchar2(128);
 text varchar2(1024);
 author varchar2(128);
 fileid number;

 like_count number := 0;
 v_str varchar2(1024);

 cursor c_likelist(in_id IN number) is select users.fullname from likes, users where lower(likes.user_id) = lower(users.username) and likes.message_id = in_id;

begin

 stmt := 'WITH message_order AS (
    SELECT id, row_number() over (ORDER BY id desc) AS rownumber
        FROM messages';

if v('P1_AUTHOR') is not null then
 stmt := stmt || ' WHERE author = ''' || v('P1_AUTHOR') || '''';
end if;

 stmt := stmt || ')
SELECT m.*
    FROM messages m
    INNER JOIN message_order mo
        ON m.id = mo.id
    WHERE mo.rownumber <= ' || :P1_SHOW || '
    ORDER BY mo.rownumber';

  open crs for stmt;
 loop 
  fetch crs into id, title, text, author, fileid;
  exit when crs%NOTFOUND;

  htp.bold(title);
  htp.p(' by <i>' || author || '</i>');
  
  -- After the title display the likes
  like_count := 0;
  v_str := '[';
  for c in c_likelist(id) loop
    like_count:=like_count+1;
    v_str := v_str||c.fullname||'&nbsp;';
  end loop;

 if ( author = :USER ) then 
  htp.p('<a href="javascript:location.reload(true)" onClick="JavaScript:var get = new htmldb_Get(null,'||:APP_ID||',''APPLICATION_PROCESS=delete_message'',1);get.addParam(''x01'',''' || id || ''');gReturn = get.get();">Delete</a>');
  else
 htp.p('<a href="javascript:location.reload(true)" onClick="JavaScript:var get = new htmldb_Get(null,'||:APP_ID||',''APPLICATION_PROCESS=like_message'',1);get.addParam(''x01'',''' || id || ''');gReturn = get.get();">Like</a>');
  end if;

  if like_count > 0 then
    htp.italic(like_count || '&nbsp;People like this ');
    htp.prn(v_str || ']<p>');
  end if;

  htp.p('<p>' || htf.escape_sc(text) || '</p>');

  if ( fileid > 0 ) then
    htp.p('<a href="f?p=&APP_ID.:3:&SESSION.::::P3_FILEID:' || fileid || '">list files</a>');
  end if;

  htp.p('<hr/>');
 end loop;

end;
